-- Your SQL goes here


INSERT INTO users (id, first_name, second_name, email, active )
VALUES (1, "Eugene", "Astuto", "eastuto@gmail.com", TRUE);

INSERT INTO agents (id, user_id)
VALUES(1,1);

INSERT INTO listings(id, address, agent_id)
VALUES (1, "114 Hedderwick Street Essendon VIC 3040", 1);