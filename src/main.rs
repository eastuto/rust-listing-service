#[macro_use]
extern crate diesel;
#[macro_use]
extern crate serde_derive;
extern crate serde_json;

#[macro_use]
extern crate gotham_derive;
extern crate serde;
extern crate dotenv;
extern crate futures;
extern crate gotham;
extern crate hyper;
extern crate mime;

pub mod schema;
pub mod models;
pub mod handlers;
pub mod db;

use diesel::prelude::*;
use gotham::router::Router;
use gotham::router::builder::{build_simple_router, DefineSingleRoute, DrawRoutes};
use self::handlers::*;

fn router() -> Router {
    build_simple_router(|route| {

        route.scope("/api", |route| {
            route.associate("/listings", |assoc| {
                assoc.post().to(repository::listing::create);
                assoc.get().to(repository::listing::get);
            });
            route
                .get("/listings/:id")
                .with_path_extractor::<repository::PathExtractor>()
                .to(repository::get_listing_handler);
            route
                .get("/agent/:id")
                .with_path_extractor::<repository::PathExtractor>()
                .to(repository::get_agent_handler);
            route
                .get("/user/:id")
                .with_path_extractor::<repository::PathExtractor>()
                .to(repository::get_user_handler);
        });

    })
}

/// Start a server and use a `Router` to dispatch requests
pub fn main() {
    let addr = "127.0.0.1:7878";
    println!("Listening for requests at http://{}", addr);

    // All incoming requests are delegated to the router for further analysis and dispatch
    gotham::start(addr, router())
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}