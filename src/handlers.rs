extern crate diesel;

use hyper::{Body, Headers, HttpVersion, Method, Response, StatusCode, Uri};
use futures::{future, Future, Stream};
use gotham::http::response::create_response;
use gotham::state::{FromState, State};

use mime;
use gotham::router::builder::{build_simple_router, DefineSingleRoute, DrawRoutes};
use gotham::handler::{HandlerFuture, IntoHandlerError};
use self::diesel::prelude::*;
use serde_derive::*;
use serde::*;
use gotham_derive::*;
use db::mysql;
use models::Listing;
use models::Agent;
use models::User;

pub mod repository {

    use super::*;
    /// Extract the main elements of the request except for the `Body`
    fn print_request_elements(state: &State) {
        let method = Method::borrow_from(state);
        let uri = Uri::borrow_from(state);
        let http_version = HttpVersion::borrow_from(state);
        let headers = Headers::borrow_from(state);
        println!("Method: {:?}", method);
        println!("URI: {:?}", uri);
        println!("HTTP Version: {:?}", http_version);
        println!("Headers: {:?}", headers);
    }

    pub mod agent {
        use super::*;
        pub fn get_db_agent(agent_id: i32) -> Agent {
            use schema::agents::dsl::*;
            let connection = mysql::establish_connection();
            let agent = agents.find(agent_id).get_result::<Agent>(&connection).expect("Error loading agent");
            agent
        }
    }

    pub mod user {
        use super::*;
        pub fn get_db_user(user_id: i32) -> User {
            use schema::users::dsl::*;
            let connection = mysql::establish_connection();
            let user = users.find(user_id).get_result::<User>(&connection).expect("Error loading user");
            user
        }
    }

    pub mod listing {
        use super::*;
        fn get_db_listings(state: &State)
        {
            use schema::listings::dsl::*;

            let connection = mysql::establish_connection();
            let results = listings
                .limit(5)
                .load::<Listing>(&connection)
                .expect("Error loading listings");
            println!("Displaying {} listings", results.len());
            for listing in results {
                println!("{}", listing.id);
                println!("----------\n");
                println!("{}", listing.agent_id);
            }
        }

        pub fn get_db_listing(listing_id: i32) -> Listing {
            use schema::listings::dsl::*;
            let connection = mysql::establish_connection();
//            let str_id = id.to_string();
            let listing = listings.find(listing_id).get_result::<Listing>(&connection).expect("Error loading listing");
            listing
        }

        /// Extracts the elements of the POST request and prints them
        pub fn create(mut state: State) -> Box<HandlerFuture> {

            let f = Body::take_from(&mut state)
                .concat2()
                .then(|full_body| match full_body {
                    Ok(valid_body) => {
                        let body_content = String::from_utf8(valid_body.to_vec()).unwrap();
                        println!("Body: {}", body_content);
                        let res = create_response(&state, StatusCode::Ok, None);
                        future::ok((state, res))
                    }
                    Err(e) => return future::err((state, e.into_handler_error())),
                });

            Box::new(f)
        }

        /// Show the GET request components by printing them.
        pub fn get(state: State) -> (State, Response) {
            print_request_elements(&state);
            get_db_listings(&state);
            let res = create_response(&state, StatusCode::Ok, None);
            (state, res)
        }
    }

    #[derive(Deserialize, StateData, StaticResponseExtender)]
    pub struct PathExtractor {
        id: String,
    }

    /// Handler function for `GET` requests directed to `/listings/:id`
    pub fn get_listing_handler(state: State) -> (State, Listing) {
        let found_listing;
        {
            let path_extractor = PathExtractor::borrow_from(&state);
            let id: i32 = path_extractor.id.parse().unwrap();
            found_listing = listing::get_db_listing(id);
        }
        (state, found_listing)
    }

    /// Handler function for `GET` requests directed to `/agent/:id`
    pub fn get_agent_handler(state: State) -> (State, Agent) {
        let found_agent;
        {
            let path_extractor = PathExtractor::borrow_from(&state);
            let id: i32 = path_extractor.id.parse().unwrap();
            found_agent = agent::get_db_agent(id);
        }
        (state, found_agent)
    }

    /// Handler function for `GET` requests directed to `/user/:id`
    pub fn get_user_handler(state: State) -> (State, User) {
        let found_user;
        {
            let path_extractor = PathExtractor::borrow_from(&state);
            let id: i32 = path_extractor.id.parse().unwrap();
            found_user = user::get_db_user(id);
        }
        (state, found_user)
    }
}
