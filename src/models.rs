
use super::schema::listings;
use super::schema::agents;
use gotham::handler::IntoResponse;
use gotham::http::response::create_response;
use gotham::state::State;
use hyper::{Response, StatusCode};
use serde_json::*;
use mime::*;

#[derive(Queryable, Associations, Serialize)]
#[belongs_to(Agent)]
pub struct Listing {
    pub id: i32,
    pub address: String,
    pub agent_id: i32
}

impl IntoResponse for Listing {
    fn into_response(self, state: &State) -> Response {
        use super::serde_json;
        use super::mime;
        create_response(
            state,
            StatusCode::Ok,
            Some((
                serde_json::to_string(&self)
                    .expect("serialized product")
                    .into_bytes(),
                mime::APPLICATION_JSON,
            )),
        )
    }
}

#[derive(Insertable)]
#[table_name="listings"]
pub struct NewListing<'a> {
    pub address: &'a str,
}

#[derive(Queryable, Associations, Serialize)]
#[belongs_to(User)]
pub struct Agent {
    pub id: i32,
    pub user_id: i32
}

impl IntoResponse for Agent {
    fn into_response(self, state: &State) -> Response {
        use super::serde_json;
        use super::mime;
        create_response(
            state,
            StatusCode::Ok,
            Some((
                serde_json::to_string(&self)
                    .expect("serialized product")
                    .into_bytes(),
                mime::APPLICATION_JSON,
            )),
        )
    }
}

// #[derive(Insertable)]
// #[table_name="authors"]
// pub struct NewAuthor<'a> {
// }


#[derive(Queryable, Associations, Serialize)]
pub struct User {
    id: i32,
    first_name: String,
    second_name: String,
    email: String,
    active: bool
}

impl IntoResponse for User {
    fn into_response(self, state: &State) -> Response {
        use super::serde_json;
        use super::mime;
        create_response(
            state,
            StatusCode::Ok,
            Some((
                serde_json::to_string(&self)
                    .expect("serialized product")
                    .into_bytes(),
                mime::APPLICATION_JSON,
            )),
        )
    }
}