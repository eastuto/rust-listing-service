table! {
    agents (id) {
        id -> Integer,
        user_id -> Integer,
    }
}

table! {
    listings (id) {
        id -> Integer,
        address -> Varchar,
        agent_id -> Integer,
    }
}

table! {
    users (id) {
        id -> Integer,
        first_name -> Varchar,
        second_name -> Varchar,
        email -> Varchar,
        active -> Bool,
    }
}

joinable!(agents -> users (user_id));
joinable!(listings -> agents (agent_id));

allow_tables_to_appear_in_same_query!(
    agents,
    listings,
    users,
);
