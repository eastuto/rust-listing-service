# Introduction

This is an Http REST service with ORM to a Mysql or Postgres database built with Rust. It includes baseline db migration script and some seed data.

# Getting started

## Dependencies

You will need:

### Rust

Install rust - https://www.rust-lang.org/tools/install.

### A database

Install MySQL or Postgres (whichever you're using) 

If Mysql then on Mac ensure you also install mysql-connector-c with

```brew install mysql-connector-c```

On Windows, you will need to use the MySQL installer to install mysql-connector-c and you may need to manually set a path for it.

## Setup Diesel

Check out http://diesel.rs/guides/getting-started/ to set up Diesel cli and commands on how to run migrations.


1. Install Diesel cli  ```cargo install diesel_cli --no-default-features --features postgres``` or ```cargo install diesel_cli --no-default-features --features mysql```
2. Create your .env with ```echo DATABASE_URL=postgres://username:password@localhost/listdb > .env``` or ```echo DATABASE_URL=mysql://username:password@localhost/listdb```
3. Run ```diesel setup``` which will create the 'listdb' database and migrations folder if didn't exist (but it does in this project)
4. Run the migration scripts with ```diesel migration run```
5. 

# Start project 

```cargo run ```

Try a GET request

```http://127.0.0.1:7878/api/listings/1```

# Questions ? ( ͡◉◞ ͜ʖ◟ ͡◉)

Email Eugene Astuto eastuto@gmail.com
